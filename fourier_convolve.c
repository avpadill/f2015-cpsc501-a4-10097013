#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define PI         3.141592653589793
#define TWO_PI     (2.0 * PI)
#define SWAP(a,b)  tempr=(a);(a)=(b);(b)=tempr
#define PADDEDSAMPLES  1048576

/**
 *
 *  CPSC 501 Fall 2015
 *  Assignment 4 : Optimizing Program Performance
 *  Submitted by Arnold Padillo
 *  UCID 10097013
 *
 *  fourier_convolve.c : 
 *      - Convolves 2 Wav Files
 *      - Dry Recording of 10 Seconds (Tested with Guitar.wav)
 *      - Impulse Response of 2 Seconds (Tested with Auto Park IR.wav)
 *
 *	This assignment heavily borrows from source code provided by
 *	the TA and from "Numerical Recipes in C", one of the course
 *  textbooks. A version of the Fast-Fourier Transform is implemented 
 *  within.
 *
 */

typedef struct wavStructs {
	char chunkID[5];
	int chunkSize;
	char format[5];

	char subChunk1ID[5];
	int subChunk1Size;
	short audioFormat;
	short numChannels;
	int sampleRate;
	int byteRate;
	short blockAlign;
	short bitsPerSample;

	int channelSize;
	char subChunk2ID[5];
	int subChunk2Size;

	double* data;
	double* fourierData;

} wavStruct;

void four1(double data[], int nn, int isign); // nn = half of what the padded length is 

clock_t before;
double elapsed;


int loadWave(char* filename, wavStruct *wav)
{
	FILE* in = fopen(filename, "rb");

	if (in != NULL)
	{		
		printf("Reading %s...\n",filename);

		fread(wav->chunkID, 1, 4, in);
		fread(&wav->chunkSize, 1, 4, in);
		fread(wav->format, 1, 4, in);

		fread(wav->subChunk1ID, 1, 4, in);
		fread(&wav->subChunk1Size, 1, 4, in);
		fread(&wav->audioFormat, 1, 2, in);
		fread(&wav->numChannels, 1, 2, in);
		fread(&wav->sampleRate, 1, 4, in);
		fread(&wav->byteRate, 1, 4, in);
		fread(&wav->blockAlign, 1, 2, in);
		fread(&wav->bitsPerSample, 1, 2, in);

		if(wav->subChunk1Size == 18)
		{
			double empty;
			fread(&empty, 1, 2, in);		
		}
		
		fread(wav->subChunk2ID, 1, 4, in);
		fread(&wav->subChunk2Size, 1, 4, in);

		int bytesPerSample = wav->bitsPerSample/8;
		int numSamples = wav->subChunk2Size / bytesPerSample;
		int complexSamples = numSamples + numSamples;

		wav->data = (double*) malloc(sizeof(double) * numSamples);

		printf("PADDED SAMPLES : %d \n", PADDEDSAMPLES);

		wav->fourierData = (double* ) malloc(sizeof(double) * PADDEDSAMPLES);
		
		int i=0;
		short sample=0;
		while(fread(&sample, 1, bytesPerSample, in) == bytesPerSample)
		{		
			wav->data[i++] = sample;
			sample = 0;			
		}

		int j,jj;

		// Inserting first segment of Fourier Data (Indexes from 0 to 882 008)
		for (j = 0, jj = 0; j < numSamples; j++, jj += 2) {
 			wav->fourierData[jj] = wav->data[j];
 			wav->fourierData[jj+1] = 0.0;
     	}

    	for(int i = numSamples + numSamples; i < PADDEDSAMPLES; i+=2){
			wav->fourierData[i] = 0.0;
			wav->fourierData[i+1] = 0.0;		
    	}

    	if(((wav->fourierData[0] == -95.0) && (wav->fourierData[40] == 21.0) && (wav->fourierData[230] == 30.0)) || ((wav->fourierData[0] == 1.0) && (wav->fourierData[40] == 1.0) && (wav->fourierData[230] == 0.0))){
    		printf("\nRandom Data Checks have PASSED\n");
    	}else{
    		printf("\nRandom Data Checks have FAILED\n");
    	}
		
		fclose(in);
		printf("Closing %s...\n",filename);
    	

	} else {
		printf("Can't open file: %s\n", filename);
		return 0;
	}

	

	return 1;
}

int saveWave(char* filename, wavStruct *wav, wavStruct *wav2)
{
	FILE* out = fopen(filename, "wb");

	if (out != NULL)
	{		
		printf("\nWriting %s...\n",filename);

		fwrite(wav->chunkID, 1, 4, out);
		fwrite(&wav->chunkSize, 1, 4, out);
		fwrite(wav->format, 1, 4, out);

		fwrite(wav->subChunk1ID, 1, 4, out);
		fwrite(&wav->subChunk1Size, 1, 4, out);
		fwrite(&wav->audioFormat, 1, 2, out);
		fwrite(&wav->numChannels, 1, 2, out);
		fwrite(&wav->sampleRate, 1, 4, out);
		fwrite(&wav->byteRate, 1, 4, out);
		fwrite(&wav->blockAlign, 1, 2, out);
		fwrite(&wav->bitsPerSample, 1, 2, out);		
		
		if(wav->subChunk1Size == 18)
		{
			short empty = 0;
			fwrite(&empty, 1, 2, out);		
		}
		
		fwrite(wav->subChunk2ID, 1, 4, out);
		fwrite(&wav->subChunk2Size, 1, 4, out);

		int bytesPerSample = wav->bitsPerSample / 8;
		int sampleCount =  wav->subChunk2Size / bytesPerSample;
		
		double* newData = (double*) malloc(sizeof(double) * (1048576));
		float maxSample = -1;
		float MAX_VAL = 32767.f;
		
		four1((wav->fourierData) - 1, 524288, 1);	// Transforming Signal Data

		four1((wav2->fourierData) - 1, 524288, 1);	// Transforming IR data

		for(int i = 0; i < 1048576; i+=2){
			newData[i] = ((double)wav->fourierData[i] / MAX_VAL) * ((double)wav2->fourierData[i] / MAX_VAL) - ((double)wav->fourierData[i + 1] / MAX_VAL) * ((double)wav2->fourierData[i + 1] / MAX_VAL);
			newData[i+1] = ((double)wav->fourierData[i+1] / MAX_VAL) * ((double)wav2->fourierData[i] / MAX_VAL) + ((double)wav->fourierData[i] / MAX_VAL) * ((double)wav2->fourierData[i + 1] / MAX_VAL);
		}

		// Y[K], Pass through IFFT
		four1(newData - 1, 524288, -1);
		
		//scale and re write the data
		for(int i=0; i < 1048576; i+=2)
		{
			newData[i] = (newData[i] / (PADDEDSAMPLES * 5)) ;
			short sample = (short) (newData[i] * MAX_VAL);
			fwrite(&sample, 1, bytesPerSample, out);
		}
		
		//clean up
		free(newData);
		fclose(out);
		printf("Closing %s...\n",filename);

	}
	else
	{
		printf("Can't open file: %s\n", filename);
		return 0;
	}
	return 1;
}

void four1(double data[], int nn, int isign)
{
    unsigned long n, mmax, m, j, istep, i;
    double wtemp, wr, wpr, wpi, wi, theta;
    double tempr, tempi;

    n = nn << 1;
    j = 1;

    for (i = 1; i < n; i += 2) {
		if (j > i) {
		    SWAP(data[j], data[i]);
		    SWAP(data[j+1], data[i+1]);
		}
		m = nn;
		while (m >= 2 && j > m) {
		    j -= m;
		    m >>= 1;
		}
		j += m;
    }

    mmax = 2;
    while (n > mmax) {
		istep = mmax << 1;
		theta = isign * (6.28318530717959 / mmax);
		wtemp = sin(0.5 * theta);
		wpr = -2.0 * wtemp * wtemp;
		wpi = sin(theta);
		wr = 1.0;
		wi = 0.0;
		for (m = 1; m < mmax; m += 2) {
		    for (i = m; i <= n; i += istep) {
				j = i + mmax;
				tempr = wr * data[j] - wi * data[j+1];
				tempi = wr * data[j+1] + wi * data[j];
				data[j] = data[i] - tempr;
				data[j+1] = data[i+1] - tempi;
				data[i] += tempr;
				data[i+1] += tempi;
		    }
		    wr = (wtemp = wr) * wpr - wi * wpi + wr;
		    wi = wi * wpr + wtemp * wpi + wi;
		}
		mmax = istep;
    }
}

int main(int argc, char* argv[])
{
	wavStruct *dryOut = (wavStruct *)malloc(sizeof(wavStruct));
	wavStruct *irOut = (wavStruct *)malloc(sizeof(wavStruct));
	char* dryName = argv[1];
	char* irName = argv[2];
	char* outFilename = "out.wav";
	
	if(argc == 4)
		outFilename = argv[3];
	else if(argc != 3)
	{
		fprintf(stderr, "Usage: convolve <inputfile> <Impulse Response File> <outputfile>\n");
		exit(-1);
	}

	// Mounting the WavData into their respective structs
	loadWave(dryName,dryOut);
	loadWave(irName,irOut);

	int i;

	before = clock();
	saveWave(outFilename,dryOut,irOut);
	elapsed = clock() - before;

	printf("Function `saveWave` used %.3f seconds \n", elapsed/CLOCKS_PER_SEC);

	free(dryOut->data);
	free(irOut->data);
	free(dryOut);
	free(irOut);
	
	return 0;

}
