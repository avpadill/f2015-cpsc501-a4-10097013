#include <stdio.h>
#include <stdlib.h>

/**
 *
 *  CPSC 501 Fall 2015
 *  Assignment 4 : Optimizing Program Performance
 *  Submitted by Arnold Padillo
 *  UCID 10097013
 *
 *  convolve.c : 
 *      - Convolves 2 Wav Files
 *      - Dry Recording of 10 Seconds (Tested with Guitar.wav)
 *      - Impulse Response of 2 Seconds (Tested with Auto Park IR.wav)
 *
 *	This assignment heavily borrows from source code provided by
 *	the TA. Time-domain convolution is implemented within.
 *
 */

typedef struct wavStructs {
	char chunkID[5];
	int chunkSize;
	char format[5];

	char subChunk1ID[5];
	int subChunk1Size;
	short audioFormat;
	short numChannels;
	int sampleRate;
	int byteRate;
	short blockAlign;
	short bitsPerSample;

	int channelSize;
	char subChunk2ID[5];
	int subChunk2Size;

	double* data;

} wavStruct;

int loadWave(char* filename, wavStruct *wav)
{
	FILE* in = fopen(filename, "rb");

	if (in != NULL)
	{		
		printf("Reading %s...\n",filename);

		fread(wav->chunkID, 1, 4, in);
		fread(&wav->chunkSize, 1, 4, in);
		fread(wav->format, 1, 4, in);

		fread(wav->subChunk1ID, 1, 4, in);
		fread(&wav->subChunk1Size, 1, 4, in);
		fread(&wav->audioFormat, 1, 2, in);
		fread(&wav->numChannels, 1, 2, in);
		fread(&wav->sampleRate, 1, 4, in);
		fread(&wav->byteRate, 1, 4, in);
		fread(&wav->blockAlign, 1, 2, in);
		fread(&wav->bitsPerSample, 1, 2, in);

		if(wav->subChunk1Size == 18)
		{
			double empty;
			fread(&empty, 1, 2, in);		
		}
		
		fread(wav->subChunk2ID, 1, 4, in);
		fread(&wav->subChunk2Size, 1, 4, in);

		int bytesPerSample = wav->bitsPerSample/8;
		int numSamples = wav->subChunk2Size / bytesPerSample;
		wav->data = (double*) malloc(sizeof(double) * numSamples);
		
		int i=0;
		short sample=0;
		while(fread(&sample, 1, bytesPerSample, in) == bytesPerSample)
		{		
			wav->data[i++] = sample;
			sample = 0;			
		}
		
		fclose(in);
		printf("Closing %s...\n",filename);

	} else {
		printf("Can't open file: %s\n", filename);
		return 0;
	}

	return 1;
}

int saveWave(char* filename, wavStruct *wav, wavStruct *wav2)
{
	FILE* out = fopen(filename, "wb");

	if (out != NULL)
	{		
		printf("\nWriting %s...\n",filename);

		fwrite(wav->chunkID, 1, 4, out);
		fwrite(&wav->chunkSize, 1, 4, out);
		fwrite(wav->format, 1, 4, out);

		fwrite(wav->subChunk1ID, 1, 4, out);
		fwrite(&wav->subChunk1Size, 1, 4, out);
		fwrite(&wav->audioFormat, 1, 2, out);
		fwrite(&wav->numChannels, 1, 2, out);
		fwrite(&wav->sampleRate, 1, 4, out);
		fwrite(&wav->byteRate, 1, 4, out);
		fwrite(&wav->blockAlign, 1, 2, out);
		fwrite(&wav->bitsPerSample, 1, 2, out);		
		
		if(wav->subChunk1Size == 18)
		{
			short empty = 0;
			fwrite(&empty, 1, 2, out);		
		}
		
		fwrite(wav->subChunk2ID, 1, 4, out);
		fwrite(&wav->subChunk2Size, 1, 4, out);

		int bytesPerSample = wav->bitsPerSample / 8;
		int bytesPerSampleIR = wav2->bitsPerSample / 8;
		int drySampleCount =  wav->subChunk2Size / bytesPerSample;
		int IRsampleCount = wav2->subChunk2Size / bytesPerSampleIR;

		int overallSampleCount = drySampleCount + IRsampleCount - 1;

		float* newData = (float*) malloc(sizeof(float) * (overallSampleCount));
		float maxSample = -1;
		float MAX_VAL = 32767.f;
			
		for(int i=0; i<drySampleCount; ++i)
		{			
			for(int j=0; j<IRsampleCount; ++j)
				newData[i+j] += ((float)wav->data[i] / MAX_VAL) * ((float)wav2->data[j] / MAX_VAL);
			
			if(i==0)
				maxSample = newData[0];
			else if(newData[i] > maxSample)
				maxSample = newData[i];
		}		
		
		for(int i=0; i < overallSampleCount; ++i)
		{
			newData[i] = (newData[i] / maxSample) ;
			short sample = (short) (newData[i] * MAX_VAL);
			fwrite(&sample, 1, bytesPerSample, out);
		}
		
		free(newData);
		fclose(out);
		printf("Closing %s...\n",filename);
	}
	else
	{
		printf("Can't open file: %s\n", filename);
		return 0;
	}
	return 1;
}

int main(int argc, char* argv[])
{
	wavStruct *dryRecording = (wavStruct *)malloc(sizeof(wavStruct));
	wavStruct *impulseResponse = (wavStruct *)malloc(sizeof(wavStruct));

	char* dryRecordingName = argv[1];
	char* impulseResponseName = argv[2];
	char* outputFilename = "out.wav";
	
	if(argc == 4)
		outputFilename = argv[3];
	else if(argc != 3)
	{
		fprintf(stderr, "Usage: convolve <inputfile> <Impulse Response File> <outputfile>\n");
		exit(-1);
	}

	// Mounting the wavFiles to their respective structures
	loadWave(dryRecordingName, dryRecording);
	loadWave(impulseResponseName, impulseResponse);

	saveWave(outputFilename, dryRecording ,impulseResponse);

	free(dryRecording);
	free(impulseResponse);
	
	return 0;

}